#include <stdio.h>
int
main ()
{
  struct employee
  {
    int empid;
    char emp_name[20];
    char doj[20];
    float emp_sal;
    char designation[100];
  } s1;
  printf ("enter employee id number:\n");
  scanf ("%d", &s1.empid);
  printf ("enter employee name:\n");
  scanf ("%s", s1.emp_name);
  printf ("enter date of joining\n");
  scanf ("%s", s1.doj);
  printf ("salary of employee\n");
  scanf ("%f", &s1.emp_sal);
  printf ("enter designation\n");
  scanf ("%s", s1.designation);
  printf ("employee details are:\n");
  printf ("empid\t name\t doj\t salary\t designation\n");
  printf ("%d\t %s\t %s\t %f\t %s", s1.empid, s1.emp_name, s1.doj, s1.emp_sal,
	  s1.designation);
  return 0;
}
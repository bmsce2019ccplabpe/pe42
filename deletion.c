#include<stdio.h>
int main ()
{
  int a[20], i, pos, ele, n;
  printf ("Enter the size of the array\n");
  scanf ("%d", &n);
  printf ("Enter the elements of the array\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  printf ("Enter the postion to be deleted\n");
  scanf ("%d", &pos);
  for (i = pos; i < n; i++)
  {
    a[i] = a[i + 1];
  n--;
  }
  printf ("The array after deletion is  ");
  for (i = 0; i < n; i++)
    printf ("%d  ", a[i]);
  return 0;
}
#include<stdio.h>
int input()
{
float r;
printf("Enter the radius\n");
scanf("%f",&r);
return r;
}
float compute_area(float r)
{
float area;
area=3.14*r*r;
return area;
}
float compute_circumference(float r)
{
float circumference;
circumference=2*3.14*r;
return circumference;
}
int output(float r,float area,float circumference)
{
printf("Area of the circle=%f",area);
printf("Circumferece of a circle=%f",circumference);
}
int main()
{
float radius,area,circumference;
radius=input();
area=compute_area(radius);
circumference=compute_circumference(radius);
output(radius,area,circumference);
return 0;
}
#include<stdio.h>
int
read (int a[])
{
  int i, n;
  printf ("Enter the size of the array\n");
  scanf ("%d", &n);
  printf ("Enter the elements of the array\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  return n;
}

void
compute (int n, int a[])
{
  int s, l, lp, sp, temp, i;
  s = a[0];
  l = a[0];
  sp = 0;
  lp = 0;
  for (i = 1; i < n; i++)
    {
      if (a[i] < s)
	{
	  s = a[i];
	  sp = i;
	}
      if (a[i] > l)
	{
	  l = a[i];
	  lp = i;
	}
    }
  printf ("smallest = %d\n", s);
  printf ("largest = %d\n", l);
  temp = a[lp];
  a[lp] = a[sp];
  a[sp] = temp;
}

void
display (int n, int a[])
{
  int i;
  printf ("The new array is\n");
  for (i = 0; i < n; i++)
    printf ("%d  ", a[i]);
}

int
main ()
{
  int a[20], x;
  x = read (a);
  compute (x, a);
  display (x, a);
  return 0;
}